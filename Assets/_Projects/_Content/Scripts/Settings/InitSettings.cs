using System;
using System.Collections.Generic;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    [Serializable]
    public class InitSettings
    {
        public PlayerSettings PlayerSettings;
        public PathSettings PathSettings;
        public UISettings UiSettings;
        public TimeSettings TimeSettings;
        public SoundSettings SoundSettings;
        public DecoratorSettings DecoratorSettings;
        public SaveableSettings SaveableSettings;
        
        [HideInInspector] 
        public Camera Camera;

        [HideInInspector] 
        public AsyncProcessor AsyncProcessor;

    }
}