using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    [CreateAssetMenu(fileName = "SaveableSettings", menuName = "Sacrificial/SaveableSettings")]
    public class SaveableSettings : SaveableScriptableObject
    {
        public int BestScore = 0;

        public void InitOnNewDevice()
        {
            if (!PlayerPrefs.HasKey("BestScore"))
            {
                BestScore = 0;
            }
        }
    }
}