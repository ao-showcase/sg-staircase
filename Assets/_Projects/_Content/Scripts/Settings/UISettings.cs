using UnityEngine;

namespace sg.staircase
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "Sacrificial/UISettings")]
    public class UISettings : ScriptableObject
    {
        [Tooltip("Префаб очков для сцены")] 
        public UIScoreInScene UiScoreInScene;

        [Tooltip("Смещение очков по X")] [Range(-20, 20)]
        public float scoreOffsetByX = 0;
        
        [Tooltip("Смещение очков по Y")] [Range(-20, 20)]
        public float scoreOffsetByY = 0;

        [Tooltip("Время показа счета")] 
        public float scoreHideTimer = 1;
    }
}