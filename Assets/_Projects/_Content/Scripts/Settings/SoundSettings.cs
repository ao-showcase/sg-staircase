using UnityEngine;

namespace sg.staircase
{
    [CreateAssetMenu(fileName = "SoundSettings", menuName = "Sacrificial/SoundSettings")]
    public class SoundSettings : ScriptableObject 
    {
        public DontDestroySounds DontDestroySounds;
        
    }
}