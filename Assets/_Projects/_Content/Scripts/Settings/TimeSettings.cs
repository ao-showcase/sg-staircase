using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace sg.staircase
{
    [CreateAssetMenu(fileName = "Time Settings", menuName = "Sacrificial/TimeSettings")]
    public class TimeSettings : ScriptableObject
    {
        [Title("Настройки времени для бонуса")]
        public AnimationCurve timeScaleInCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        public AnimationCurve timeScaleOutCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        public float slowdownValue = 0.2f;
        public float slowdownDuration = 1.0f;

        [Title("Настройки времени для смерти")]

        public bool UserDeathTimeScale = false;
        
        public AnimationCurve timeScaleOutCurveDeath = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        public float slowdownValueDeath = 0.2f;
        public float slowdownDurationDeath = 1.0f;
        
    }
}