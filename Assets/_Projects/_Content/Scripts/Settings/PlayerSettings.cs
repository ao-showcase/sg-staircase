using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace sg.staircase
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "Sacrificial/PlayerSettings")]
    public class PlayerSettings : ScriptableObject
    {
        public float JumpTime = 0.2f;
        public float BaseJumpForce = 300;

        [Title("Настройки падения с платформы")]

        [Tooltip("Степень отскока сторону во время отлета")]
        [Range(0, 2)]
        public float FallSideDistance = 1;

        [Tooltip("Время отскока в сторону во время падения")] 
        public float FallSideDuration = 1;
        
        [Tooltip("Время кручения во время падения")] 
        public float RotateSideDuration = 1;
       
        [Title("Кривая прыжка")]
        
        [Tooltip("Тип кривой прыжка")]
        public Ease Ease;
        
        [Tooltip("Кастомная кривая прыжка")]
        public AnimationCurve EaseCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
    }
}