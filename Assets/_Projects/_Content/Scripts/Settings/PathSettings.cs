using System.Collections.Generic;
using UnityEngine;

namespace sg.staircase
{
    [CreateAssetMenu(fileName = "PathSettings", menuName = "Sacrificial/PathSettings")]
    public class PathSettings : ScriptableObject
    {
        [Tooltip("Останавливать платформы во время смерти")]
        public bool StopPlatformsWhenDead = false;
        
        [Tooltip("Засчитывать попадание на платформу если персонаж упал на бок платформы")]
        public bool LandedOnStepSide = false;
        
        [Tooltip("Префаб платформы")] 
        public Step StepPrefab;
        
        [Tooltip("Расстояние между рядами")] 
        public float StepX;
        
        [Tooltip("Начальная скорость платформ")] 
        public float StepsStartSpeed = 1;
        
        [Tooltip("Инкремент скорости платформ (на сколько ускоряется при каждом прыжке)")] 
        public float SpeedIncrement = 1;

        [Tooltip("Умножитель инкремента")] 
        [Range(0, 1)]
        public float IncrementMultiplier = 1;

        [Tooltip("Максимальная скорость")] 
        public float MaxVelocity = 100;
        
        [Tooltip("Расстояние между платформами в ряду")] 
        public float StepWidthInRow = 2;
        
        [Tooltip("Число платформ в ряду")] 
        public int CountInRow = 5;
        
        [Tooltip("Число рядов, просчитанных наперёд")]
        public int RowCount = 5;
        
        [Header("Рандомное смещение платформ в ряду по Z")] 
        [MinMaxSlider(0f, 1f)] 
        public Vector2 RandomOffsetByZ;

        [Header("Ширина платформ")]
        [MinMaxSlider(0.1f, 5)] 
        public Vector2 StepWidthRange;
        
        [Header("Время появления платформ, с которых спрыгнули")]
        [MinMaxSlider(0.0f, 1.0f)] 
        public Vector2 WakeDelayRange;
        
        [Header("Время уничтожения платформ, с которых спрыгнули")]
        [MinMaxSlider(0.0f, 1.0f)] 
        public Vector2 DestroyDelayRange;

        [Tooltip("Во сколько раз замедлятся или ускорятся платформы при падении игрока")]
        public float PlayerFallVelocityFactor = 0;

        [Tooltip("Step Base материалы")] 
        public List<Material> StepBaseMaterials;
        
        [Tooltip("Step Side материалы")] 
        public List<Material> StepSideMaterials;
        
        [Tooltip("Материалы назначаемые игроку")]
        public List<StepColorForPlayer> MaterialsForPlayer;

        [Header("БОНУСЫ")]
        
        [Tooltip("Префабы бонусов")] 
        public List<BonusWithProbability> Bonuses;

        [Tooltip("Вероятность появления бонусов")] 
        [Range(0.0f, 1.0f)]
        public float DropChance;

        [Tooltip("Combo умножитель")] 
        public int ComboMultiplier = 2;
    }
}