using System;
using System.Collections.Generic;
using UnityEngine;

namespace sg.staircase
{
    [Serializable]
    public class DecoratorMaterial
    {
        public Material BaseMaterial;
        public Material SideMaterial;
    }
    
    [Serializable]
    public class DecoratorAnimationData
    {
        [MinMaxSlider(-15f, 0.0f)] public Vector2 HeightRange;
        [MinMaxSlider(-15f, 15f)] public Vector2 ZOffsetRange;
        [MinMaxSlider(0f, 360f)] public Vector2 RotationRange;

        [MinMaxSlider(0f, 3f)] public Vector2 ScaleToRange;
        [MinMaxSlider(0f, 2f)] public Vector2 DurationRange;

        public List<AnimationCurve> AnimationCurves = new List<AnimationCurve>
        {
            new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1))
        };
    }
    
    [Serializable]
    public struct DecoratorData
    {
        public Decorator DecoratorPrefab;
        public DecoratorAnimationData DecoratorAnimationData;
    }
    
    [CreateAssetMenu(fileName = "DecoratorSettings", menuName = "Sacrificial/DecoratorSettings")]
    public class DecoratorSettings : ScriptableObject
    {
        [Tooltip("Список декораторов")]
        public List<DecoratorData> Decorators = new List<DecoratorData>();

        [Tooltip("Как часто генерировать")] 
        [MinMaxIntSlider(1, 20)] public Vector2 FrequencyRange;
        
        [Tooltip("Список материалов для декораторов")]
        public List<DecoratorMaterial> DecoratorMaterials = new List<DecoratorMaterial>();
    }
}