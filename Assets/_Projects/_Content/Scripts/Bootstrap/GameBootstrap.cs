using System.Collections.Generic;
using lab.framework;
using UnityEngine;
using UnityEngine.Scripting;

[assembly: Preserve]

namespace sg.staircase
{
    public class GameBootstrap : MonoBehaviour
    {
        private readonly List<IInitializable> initializables = new List<IInitializable>();
        private readonly List<ILateInitializable> lateInitializables = new List<ILateInitializable>();
        private readonly List<ITickable> tickables = new List<ITickable>();
        private readonly List<ILateTickable> lateTickables = new List<ILateTickable>();
        private readonly List<IDisposable> disposables = new List<IDisposable>();

        
        [SerializeField] private InitSettings initSettings;

        private GameEventsSystem gameEventsSystem;
        private InputSystem inputSystem;
        private UISystem uiSystem;

        private SceneBoundsSystem sceneBoundsSystem;
        private PlayerLifeSystem playerLifeSystem;
        private LevelGenerator levelGenerator;
        private PlayerMovementSystem playerMovementSystem;
        private StepAnimationSystem stepAnimationSystem;
        private PlayerAnimationSystem playerAnimationSystem;
        private UIScoreSystem uiScoreSystem;
        private ScoreSystem scoreSystem;
        private PlayerColorSystem playerColorSystem;
        
        private BonusLifeSystem bonusLifeSystem;
        private BonusEffectorSystem bonusEffectorSystem;

        private TimeScaleSystem timeScaleSystem;
        private FXAnimateSystem fxAnimateSystem;

        private DecoratorsGeneratorSystem decoratorGenerationSystem;

        private SoundSystem soundSystem;

        private AnalyticsSystem analyticsSystem;
        
        protected void Awake ()
        {
            Application.targetFrameRate = 60;

            Cursor.visible = false;

            initSettings.Camera = Camera.main;

            initSettings.SaveableSettings.InitOnNewDevice();
            initSettings.SaveableSettings.LoadSettings();
            
            initSettings.AsyncProcessor = CreateAsyncProcessor ();

            gameEventsSystem = new GameEventsSystem();
            disposables.Add(gameEventsSystem);
            
            inputSystem = new InputSystem();
            initializables.Add(inputSystem);
            tickables.Add(inputSystem);
            disposables.Add(inputSystem);
            
            uiSystem = new UISystem(gameEventsSystem, inputSystem);
            initializables.Add(uiSystem);

            sceneBoundsSystem = new SceneBoundsSystem();
            initializables.Add(sceneBoundsSystem);
            tickables.Add(sceneBoundsSystem);
            
            playerLifeSystem = new PlayerLifeSystem(gameEventsSystem);
            initializables.Add(playerLifeSystem);
            
            levelGenerator = new LevelGenerator(playerLifeSystem, gameEventsSystem);
            initializables.Add(levelGenerator);
            lateInitializables.Add(levelGenerator);
            tickables.Add(levelGenerator);
            
            playerMovementSystem = new PlayerMovementSystem(playerLifeSystem, inputSystem, gameEventsSystem);
            initializables.Add(playerMovementSystem);
            tickables.Add(playerMovementSystem);
            
            stepAnimationSystem = new StepAnimationSystem(gameEventsSystem);
            initializables.Add(stepAnimationSystem);
            
            playerAnimationSystem = new PlayerAnimationSystem(gameEventsSystem, playerLifeSystem);
            initializables.Add(playerAnimationSystem);

            scoreSystem = new ScoreSystem(gameEventsSystem);
            initializables.Add(scoreSystem);
            
            uiScoreSystem = new UIScoreSystem(gameEventsSystem, playerMovementSystem);
            initializables.Add(uiScoreSystem);
            

            
            playerColorSystem = new PlayerColorSystem(gameEventsSystem, playerLifeSystem);
            initializables.Add(playerColorSystem);

            bonusLifeSystem = new BonusLifeSystem(gameEventsSystem);
            initializables.Add(bonusLifeSystem);
            
            bonusEffectorSystem = new BonusEffectorSystem(gameEventsSystem);
            initializables.Add(bonusLifeSystem);
            
            fxAnimateSystem = new FXAnimateSystem(gameEventsSystem, inputSystem);
            initializables.Add(fxAnimateSystem);
            disposables.Add(inputSystem);
            
            timeScaleSystem = new TimeScaleSystem(gameEventsSystem);
            initializables.Add(timeScaleSystem);
            disposables.Add(timeScaleSystem);

            decoratorGenerationSystem = FindObjectOfType<DecoratorsGeneratorSystem>();
            decoratorGenerationSystem.SetEvents(gameEventsSystem);
            initializables.Add(decoratorGenerationSystem);

            soundSystem = new SoundSystem(gameEventsSystem);
            initializables.Add(soundSystem);

            analyticsSystem = new AnalyticsSystem(gameEventsSystem);
            initializables.Add(analyticsSystem);
            disposables.Add(analyticsSystem);
            

            InitializeAll (initSettings);
            LateInitializeAll ();
        }
        
        private AsyncProcessor CreateAsyncProcessor()
        {
            GameObject asyncGO = new GameObject("Async Processor");
            AsyncProcessor asyncProcessor = asyncGO.AddComponent<AsyncProcessor>();
            return asyncProcessor;
        }
        
        private void InitializeAll(params object[] arg)
        {
            foreach (var initializable in initializables)
            {
                initializable.Initialize(arg);
            }
        }

        private void LateInitializeAll()
        {
            foreach (var lateInitializable in lateInitializables)
            {
                lateInitializable.LateInitialize();
            }
        }

        private void Update()
        {
            foreach (var tickable in tickables)
            {
                tickable.Tick();
            }      
        }

        private void LateUpdate()
        {
            foreach (var lateTickable in lateTickables)
            {
                lateTickable.LateTick();
            } 
        }

        private void OnDestroy()
        {
            foreach (var disposable in disposables)
            {
                disposable.Dispose();
            }
        }
    }
}

/*

Баги:
1. Иногда на старте приземляется не на землю, а на воздух, выше
2. Если в падении задел платформу - иногда платформы дублируются, одна накладывается на другую
3. Иногда на первых двух рядах куб чуть ниже, чем на всех остальных, и не видно тень
4. На первом прыжке иногда смещены анимации

- вынести настройки заммедления времени на смерть
- хуево перезагружается сцена
- декораторы

*/


