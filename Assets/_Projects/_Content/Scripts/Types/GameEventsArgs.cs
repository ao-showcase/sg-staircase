namespace sg.staircase
{
    public struct JumpEventArg
    {
        public int StepRowNumber { get; }
        public Step JumpFromStep { get; }

        public JumpEventArg(int stepRowNumber, Step jumpFromStep)
        {
            StepRowNumber = stepRowNumber;
            JumpFromStep = jumpFromStep;
        }
    }
}