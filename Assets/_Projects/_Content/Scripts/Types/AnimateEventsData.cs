using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace sg.staircase
{
    public enum AnimateEvent
    {
        Jump,
        Land,
        Death,
        Combo,
        
        FirstJump,
        TapOnDeathScreen,
        
        DecoratorCreated,
        BonusCatched
    }

    public enum AnimationType
    {
        Trigger,
        Bool,
        Int
    }

    public enum CameraSizeTweenAnimationType
    {
        CameraSizeOnce,
        CameraSizeCombo,
        CameraColorCombo
    }

    [Serializable]
    public struct AnimateEventData
    {
        public AnimateEvent AnimateEvent;
        public AnimationType AnimationType;
        public string ParamName;
        [ShowIf("AnimationType", AnimationType.Bool)]
        public bool BoolValue;
    }
    
    [Serializable]
    public struct CameraSizeTweenAnimationData
    {
        public AnimateEvent AnimateEvent;
        public CameraSizeTweenAnimationType AnimationType;

        [HideIf("AnimationType", CameraSizeTweenAnimationType.CameraSizeOnce)]
        public int ComboCount;
        
        public AnimationCurve tweenInCurve;
        public float toVal;
        public float time;       
    }
    
    public interface IAnimateData
    {
    }

    public class IntAnimateData : IAnimateData
    {
        public readonly int Value;

        public IntAnimateData(int value)
        {
            Value = value;
        }
    }

    public class ComboAnimateData : IAnimateData
    {
        public readonly bool ComboSuccess = false;
        public readonly ComboData ComboData;

        public ComboAnimateData(bool comboSuccess, ComboData comboData)
        {
            ComboSuccess = comboSuccess;
            ComboData = comboData;
        }
    }
}