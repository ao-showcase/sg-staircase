namespace sg.staircase
{
    public enum DeathReason
    {
        Out_Of_Screen,
        Not_Landed,
        Bad_Landed
    }
}