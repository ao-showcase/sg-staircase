using System;
using UnityEngine;

namespace sg.staircase
{
    [Serializable]
    public struct BonusWithProbability
    {
        public Bonus Bonus;
        [Tooltip("Вероятность появления бонуса")] 
        [Range(0.0f, 1.0f)]
        public float DropRarity;

        [Tooltip("Появление бонуса после дорожки с номером")]
        public int AppearanceRowNumber;
    }

    [Serializable]
    public struct StepColorForPlayer
    {
        public Color BaseColor;
        public Color SideColor;
    }
}