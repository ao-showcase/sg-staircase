using System.Collections.Generic;
using lab.framework;
using UnityEngine;
using UnityEngine.Analytics;
using UnityStandardAssets.Utility;

namespace sg.staircase
{
    public class AnalyticsSystem : BaseInitializableSystem, IDisposable
    {
        private GameEventsSystem gameEventsSystem;
        private GameAnalytics gameAnalytics;

        private int stepCounter = 0;
        private VelocityData velocityData;
        
        private FPSCounter fpsCounter;

        public AnalyticsSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            gameAnalytics = GameObject.FindObjectOfType<GameAnalytics>();

            if (!gameAnalytics)
            {
                GameObject analyticsObj = new GameObject("Game Analytics");
                gameAnalytics = analyticsObj.AddComponent<GameAnalytics>();
            }

            AnalyticsEvent.LevelStart(0);
            
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
            gameEventsSystem.OnVelocityAndIncrement += OnVelocityAndIncrement;

            fpsCounter = GameObject.FindObjectOfType<FPSCounter>();
        }

        private void OnVelocityAndIncrement(VelocityData velData)
        {
            velocityData = velData;
        }

        private void OnPlayerDied(DeathReason reason)
        {
            Dictionary<string, object> param = new Dictionary<string, object>();
            param.Add( "died_on_step", stepCounter );
            param.Add( "time_elapsed", Time.timeSinceLevelLoad );
            param.Add( "death_reason", reason.ToString() );
            param.Add( "velocity", velocityData.Velocity );
            param.Add( "velocity_increment", velocityData.Increment );

            if (fpsCounter)
            {
                param.Add( "fps", fpsCounter.GetFPS() );
            }
            
            AnalyticsEvent.Custom("player_died", param);
        }

        private void OnPlayerLanded(Step arg0, int arg1)
        {
            stepCounter++;
        }


        public void Dispose()
        {
            
        }
    }
}