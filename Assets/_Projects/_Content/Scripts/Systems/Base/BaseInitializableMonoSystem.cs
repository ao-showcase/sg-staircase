using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class BaseInitializableMonoSystem: MonoBehaviour, IInitializable
    {
        protected InitSettings InitSettings;
        
        public virtual void Initialize(params object[] arg)
        {
            InitSettings = arg[0] as InitSettings;
            if (InitSettings == null)
            {
                Debug.LogError("No InitSettings");
                return;
            }
        }
    }
}