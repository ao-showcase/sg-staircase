using System.Collections.Generic;
using lab.framework;
using UnityEngine.Events;

namespace sg.staircase
{
    public struct VelocityData
    {
        public float Velocity;
        public float Increment;
    }
    
    public class GameEventsSystem : IDisposable
    {
        public UnityAction<DeathReason> OnPlayerDied;
        public UnityAction<JumpEventArg> OnJumpToStep; // row number; previous Step

        public UnityAction OnPlayerMissed;
        public UnityAction<Step, int> OnPlayerLanded;

        public UnityAction<int, int, Step> OnScoreUpdate;

        public UnityAction<RowGenerator, int> OnRowGenerated;

        public UnityAction<Bonus> OnBonusCatched;

        public UnityAction<ComboData> OnComboDataIncreased;
        public UnityAction OnComboFailed;
        
        public UnityAction<Decorator> OnDecoratorCreated;

        public UnityAction<VelocityData> OnVelocityAndIncrement;
        
        
        public void Dispose()
        {
            OnPlayerLanded = null;
            OnPlayerDied = null;
            OnJumpToStep = null;
            OnScoreUpdate = null;
            OnRowGenerated = null;
            OnBonusCatched = null;
            OnComboDataIncreased = null;
            OnComboFailed = null;
            OnDecoratorCreated = null;
            OnVelocityAndIncrement = null;
        }
    }
}