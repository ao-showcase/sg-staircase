using UnityEngine.Events;

namespace sg.staircase
{
    public static class FxRegistrator
    {
        public static UnityAction<BaseAnimateOnEvent> OnAnimatedObjectStarted;
        public static UnityAction<BaseAnimateOnEvent> OnAnimatedObjectDestroyed;
        
        public static UnityAction<BaseSoundPlayer> OnSoundPlayerStarted;
        public static UnityAction<BaseSoundPlayer> OnSoundPlayerDestroyed;

        public static void Dispose()
        {
            OnAnimatedObjectStarted = null;
            OnAnimatedObjectDestroyed = null;
            
            OnSoundPlayerStarted = null;
            OnSoundPlayerDestroyed = null;
        }
    }
}