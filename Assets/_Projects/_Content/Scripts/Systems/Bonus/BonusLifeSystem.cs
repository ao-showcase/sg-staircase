using System.Collections.Generic;
using System.Linq;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class BonusLifeSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;

        public BonusLifeSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            gameEventsSystem.OnRowGenerated += OnRowGenerated;
        }

        private void OnRowGenerated(RowGenerator row, int id)
        {
            float maxVal = 0;
            foreach (var bonus in InitSettings.PathSettings.Bonuses)
            {
                if (bonus.DropRarity > maxVal)
                {
                    maxVal = bonus.DropRarity;
                }
            }
            
            foreach (var step in row.GeneratedStepsInRow)
            {
                System.Random rnd = new System.Random();
                List<BonusWithProbability> randomizedList =
                    (from item in InitSettings.PathSettings.Bonuses
                        orderby rnd.Next()
                        select item).ToList();
                
                float probability = Random.Range(0.0f, 1.0f);

                if (probability >= (1 - InitSettings.PathSettings.DropChance))
                {
                    float randomValue = Random.Range(0.0f, maxVal);  
                    
                    foreach (var item in randomizedList)
                    {
                        if (randomValue <= item.DropRarity && id >= item.AppearanceRowNumber)
                        {
                            CreateBonus(item.Bonus, step);
                            break;
                        }
                    }
                }
            }
        }

        private void CreateBonus(Bonus randomBonusPrefab, Step step)
        {                   
            Bonus bonus = GameObject.Instantiate(randomBonusPrefab, step.BonusHolder, true);
            bonus.transform.localPosition = Vector3.zero;
            bonus.transform.localScale = randomBonusPrefab.transform.localScale;

            BonusHit bonusHit = bonus.GetComponentInChildren<BonusHit>();
            bonusHit.OnTriggerEntered += OnTriggerEntered;
        }

        private void OnTriggerEntered(Bonus bonus, Collider other)
        {
            PlayerHit playerHit = other.GetComponent<PlayerHit>();
            
            if (playerHit && !bonus.Catched)
            {
                bonus.Catched = true;
                bonus.Hit();
                playerHit.Parent.BonusAdditionalScore = bonus.BonusScore;
                
                gameEventsSystem.OnBonusCatched?.Invoke(bonus);
            }
        }
    }
}