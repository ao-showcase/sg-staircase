using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class BonusEffectorSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;

        public BonusEffectorSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            gameEventsSystem.OnBonusCatched += OnBonusCatched;
        }

        private void OnBonusCatched(Bonus bonus)
        {
            foreach (var bonusBonusBehaviour in bonus.Effectors)
            {
                bonusBonusBehaviour.Run(InitSettings);
            }
        }
    }
}