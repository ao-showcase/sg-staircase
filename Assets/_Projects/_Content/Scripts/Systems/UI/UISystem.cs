using System;
using lab.framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using IDisposable = lab.framework.IDisposable;

namespace sg.staircase
{
    public class UISystem: BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        private InputSystem inputSystem;
        
        private GameUI gameUI;

        private string time = "";

        private bool gameStarted = false;
        private static readonly int Level = Animator.StringToHash("Level");
        private static readonly int Death = Animator.StringToHash("Death");
        private static readonly int Menu = Animator.StringToHash("Menu");

        public UISystem(GameEventsSystem gameEventsSystem, InputSystem inputSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
            this.inputSystem = inputSystem;
        }
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            gameUI = GameObject.FindObjectOfType<GameUI>();
            
            inputSystem.OnTap += OnTap;

            foreach (var restartBtn in gameUI.RestartButtons)
            {
                restartBtn.onClick.AddListener(OnRestatClick);
            }
            
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
            gameEventsSystem.OnScoreUpdate += OnScoreUpdate;

            ShowBestScore();
        }

        private void OnRestatClick()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void OnTap(float tapTime)
        {
            if (!gameStarted)
            {
                gameStarted = true;
                gameUI.StatesAnimator.SetTrigger(Level);
            }
        }
        
        private void OnPlayerDied(DeathReason deathReason)
        {
            ShowBestScore();
            
            gameUI.StatesAnimator.SetTrigger(Death);
        }
 
        private void OnScoreUpdate(int score, int addScore, Step step)
        {
            gameUI.Score.ForEach((s) => s.SetText(score + ""));
        }

        private void ShowBestScore()
        {
            gameUI.BestScore.ForEach((s) => s.SetText(InitSettings.SaveableSettings.BestScore + ""));
            gameUI.BestScoreUGUI.ForEach((s) => s.SetText(InitSettings.SaveableSettings.BestScore + ""));
        }
    }
}