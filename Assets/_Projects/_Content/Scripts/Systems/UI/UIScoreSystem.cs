using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class UIScoreSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        private PlayerMovementSystem playerMovementSystem;

        private Transform ScoreParent;

        public UIScoreSystem(GameEventsSystem gameEventsSystem, PlayerMovementSystem playerMovementSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
            this.playerMovementSystem = playerMovementSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            ScoreParent = new GameObject("Score Parent").transform;
            
            gameEventsSystem.OnScoreUpdate += OnScoreUpdate;
        }

        private void OnScoreUpdate(int score, int scoreIncrement, Step step)
        {
            UIScoreInScene uiScoreInScene = GameObject.Instantiate(InitSettings.UiSettings.UiScoreInScene, ScoreParent, true);
            uiScoreInScene.transform.position = playerMovementSystem.CurrentPlayerPosition;

            int shownScore = scoreIncrement;
            
            uiScoreInScene.Init(shownScore, InitSettings.UiSettings.scoreHideTimer, step.Color);
        }
    }
}