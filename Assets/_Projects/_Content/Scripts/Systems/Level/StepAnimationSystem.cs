using System.Collections;
using DG.Tweening;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class StepAnimationSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        private static readonly int hitParam = Animator.StringToHash("Hit");

        public StepAnimationSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
        }

        private void OnPlayerLanded(Step step, int addScore)
        {            
            InitSettings.AsyncProcessor.StartCoroutine(HitDelayed(step, addScore));
        }

        private IEnumerator HitDelayed(Step step, int addScore)
        {
            yield return new WaitForEndOfFrame();
            
            if (!step.PlayerOnStep && step.Animator)
            {
                
                step.Animator.SetTrigger(hitParam);
                step.PlayerOnStep = true;
            }
        }

        
    }
}