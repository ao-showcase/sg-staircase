using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace sg.staircase
{
    public class RowGenerator
    {
        public List<Step> GeneratedStepsInRow => steps;
        private int rowNumber;
        
        private InitSettings initSettings;
        private float startPosZ;
        private int sign = 1;
        
        private List<Step> steps = new List<Step>();

        private Transform parent;

        private bool veryFirstStep;

        private bool destroyed = false;

        private bool canMove = true;
        
        public RowGenerator(InitSettings initSettings, bool veryFirstStep, Transform parent)
        {
            this.veryFirstStep = veryFirstStep;
            this.initSettings = initSettings;
            this.parent = parent;
        }
        
        public void Generate(float posX, int count, int id, bool autoWake)
        {
            rowNumber = id;
            
            sign = id % 2 == 0 ? 1 : -1;
            
            startPosZ = -count / 2 * initSettings.PathSettings.StepWidthInRow;
            
            for (int i = 0; i < count; i++)
            {
                Step step = GameObject.Instantiate(initSettings.PathSettings.StepPrefab);

                float randomOffsetByZ = id == 0 ? 0 : Random.Range(initSettings.PathSettings.RandomOffsetByZ.x, initSettings.PathSettings.RandomOffsetByZ.y);
                int randomSign = Random.Range(0.0f, 1.0f) > 0.5f ? -1 : 1;
                
                step.transform.position = new Vector3(posX, 0, startPosZ + i * initSettings.PathSettings.StepWidthInRow + randomSign * randomOffsetByZ);
                
                step.transform.localScale = new Vector3(step.transform.localScale.x, step.transform.localScale.y,
                                                        Random.Range(initSettings.PathSettings.StepWidthRange.x, initSettings.PathSettings.StepWidthRange.y));

                step.transform.parent = parent;
                
                steps.Add(step);

                var hits = step.GetComponentsInChildren<StepHit>().ToList();
                hits.ForEach((hit) =>
                {
                    hit.Parent = step;
                    hit.OnTriggerExited += OnTriggerExited;
                });

                if (autoWake)
                {
                    initSettings.AsyncProcessor.StartCoroutine(RandomWakeWithDelay(step));
                }

                if (veryFirstStep)
                {
                    step.FirstStep = true;
                    InitWithoutAnimator(step);
                    //SwitchOffRendering(step);
                }
                else
                {
                    step.FirstStep = false;
                }

                SetRandomMaterial(step);
            }
        }

        private void OnTriggerExited(Step step, Collider other)
        {
            if (other == null || step == null || steps.Count == 0)
                return;

            if (!other.gameObject.tag.Equals("Path Bounds"))
                return;

            if (steps[steps.Count - 1] == null)
                return;
                        
            if (steps[0] == step && sign == -1)
            {
                steps.RemoveAt(0);
                
                
                float randomOffsetByZ = Random.Range(initSettings.PathSettings.RandomOffsetByZ.x, initSettings.PathSettings.RandomOffsetByZ.y);
                int randomSign = Random.Range(0.0f, 1.0f) > 0.5f ? -1 : 1;
                
                step.transform.position = steps[steps.Count - 1].transform.position + new Vector3(0, 0, initSettings.PathSettings.StepWidthInRow  + randomSign * randomOffsetByZ);
                
                steps.Add(step);
            }
            else if (steps[steps.Count - 1] == step && sign == 1)
            {
                steps.RemoveAt(steps.Count - 1);
                
                float randomOffsetByZ = Random.Range(initSettings.PathSettings.RandomOffsetByZ.x, initSettings.PathSettings.RandomOffsetByZ.y);
                int randomSign = Random.Range(0.0f, 1.0f) > 0.5f ? -1 : 1;
                
                step.transform.position = steps[0].transform.position - new Vector3(0, 0, initSettings.PathSettings.StepWidthInRow  + randomSign * randomOffsetByZ);
                steps.Insert(0, step);
            }
        }

        public void Move(float velocity)
        {
            if (veryFirstStep || !canMove)
                return;
            
            steps.ForEach((step) =>
            {
                //step.transform.Translate(sign * Vector3.forward * Time.deltaTime * velocity);
                float s = velocity * Time.deltaTime; // calculate distance to move
                Vector3 target = step.transform.position + sign * Vector3.forward;
                
                step.transform.position = Vector3.MoveTowards(step.transform.position, target, s);
            });
        }

        public void HideAll()
        {
            if (destroyed)
                return;

            destroyed = true;
            initSettings.AsyncProcessor.StartCoroutine(RandomDestroyWithDelay(null, false));
        }

        public void DestroyAll(Step prevStep)
        {
            if (destroyed)
                return;

            destroyed = true;
            initSettings.AsyncProcessor.StartCoroutine(RandomDestroyWithDelay(prevStep, true));
        }

        public void WakeAll()
        {
            foreach (var step in steps)
            {
                initSettings.AsyncProcessor.StartCoroutine(RandomWakeWithDelay(step));
            }
        }

        public void StopAll()
        {
            canMove = false;
        }

        private IEnumerator RandomWakeWithDelay(Step step)
        {
            yield return new WaitForSeconds(Random.Range(initSettings.PathSettings.WakeDelayRange.x,initSettings.PathSettings.WakeDelayRange.y));

            if (step.Animator)
            {
                step.Animator.SetTrigger("Wake");
            }
        }
        
        private IEnumerator RandomDestroyWithDelay(Step prevStep, bool firstStepConcrete)
        {
            yield return new WaitForEndOfFrame();
            
            if (firstStepConcrete)
            {
                var animator = steps.Find(p => p == prevStep).Animator;
                if (animator != null)
                    animator.SetTrigger("Hide");
                steps.Remove(prevStep);
                
                var seed = (Time.time + "").GetHashCode();
            
                var random = new System.Random(seed);
            
                yield return new WaitForSeconds(Random.Range(initSettings.PathSettings.DestroyDelayRange.x,initSettings.PathSettings.DestroyDelayRange.y ));
                steps = steps.OrderBy(x => random.Next()).ToList();

            }
            
            for (int i = steps.Count - 1; i >= 0; i--)
            {
                if (i < steps.Count && steps[i])
                {
                    if (steps[i].Animator)
                    {
                        steps[i].Animator.SetTrigger("Hide");
                    }

                    steps.RemoveAt(i);

                    yield return new WaitForSeconds(Random.Range(initSettings.PathSettings.DestroyDelayRange.x,
                        initSettings.PathSettings.DestroyDelayRange.y));
                }

                yield return null;
            }
            
            steps.Clear();
        }

        private void SwitchOffRendering(Step step)
        {
            foreach (var renderer in step.GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = false;
            }
        }

        private void InitWithoutAnimator(Step step)
        {
            GameObject.Destroy(step.Animator);
            step.Pivot.localScale = Vector3.zero;
        }

        private void SetRandomMaterial(Step step)
        {
            int i = Random.Range(0, initSettings.PathSettings.StepBaseMaterials.Count);
            //int i = Random.Range(0, 1);
            
            foreach (var renderer in step.GetComponentsInChildren<Renderer>())
            {
                step.ColorID = i + 1;
               
                step.Color = initSettings.PathSettings.StepBaseMaterials[i].GetColor("_BaseColor");
                step.SideColor = initSettings.PathSettings.StepSideMaterials[i].GetColor("_BaseColor");

                if (i < initSettings.PathSettings.MaterialsForPlayer.Count)
                {
                    step.StepColorForPlayer = initSettings.PathSettings.MaterialsForPlayer[i];
                }

                if (renderer.CompareTag("Step"))
                {

                    Material[] mats = renderer.materials;

                    mats[1] = initSettings.PathSettings.StepBaseMaterials[i];
                    mats[0] = initSettings.PathSettings.StepSideMaterials[i];

                    renderer.materials = mats;
                }
            }
        }

    }
}