using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class ScoreSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        private int currentScore;
        private int scoreIncrement = 1;
        private bool firstLand = true;
        private int prevComboCount = 0;

        private ScoreComboProcessor scoreComboProcessor;

        public ScoreSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            scoreComboProcessor = new ScoreComboProcessor();
            
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
        }

        private void OnPlayerDied(DeathReason arg0)
        {
            if (currentScore > InitSettings.SaveableSettings.BestScore)
            {
                InitSettings.SaveableSettings.BestScore = currentScore;
                InitSettings.SaveableSettings.SaveSettings();
            }
        }

        private void OnPlayerLanded(Step step, int bonusScore)
        {
            if (firstLand)
            {
                firstLand = false;
                return;
            }

            ComboData comboData = scoreComboProcessor.ComboBonus(step, InitSettings.PathSettings.ComboMultiplier);
            int comboScore = comboData.Score;           
            gameEventsSystem.OnComboDataIncreased?.Invoke(comboData);

            if (prevComboCount != 0 && comboData.Counter == 0)
            {
                gameEventsSystem.OnComboFailed?.Invoke();
            }

            prevComboCount = comboData.Counter;
            
            int incr = scoreIncrement * comboScore + bonusScore;
            currentScore += incr;
                        
            gameEventsSystem.OnScoreUpdate?.Invoke(currentScore, incr, step);
        }
    }
}