using System.Collections.Generic;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class DecoratorsGeneratorSystem : BaseInitializableMonoSystem
    {
        private GameEventsSystem gameEventsSystem;
        
        public DecoratorSettings DecoratorSettings;
        public PathSettings PathSettings;
        
        public bool TestMode = false;
        public bool RemoveLast = false;
        public bool RandomMaterial = false;
        
        private List<Decorator> decorators = new List<Decorator>();

        private int landedCounter = 0;
        private int nextDecoratorsFrequency = 1;

        public void SetEvents(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            SetDecoratorsFrequency();
            
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
        }

        private void OnPlayerLanded(Step step, int arg1)
        {
            if (landedCounter > 0)
            {                
                if (landedCounter % nextDecoratorsFrequency == 0)
                {
                    SetDecoratorsFrequency();
                    Generate(step, true);
                }
            }
            
            landedCounter++;
        }

        private void SetDecoratorsFrequency()
        {
            nextDecoratorsFrequency =
                (int)Random.Range(DecoratorSettings.FrequencyRange.x, DecoratorSettings.FrequencyRange.y);
        }

        private void Update()
        {
            if (!TestMode)
                return;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Generate(null, false);
            }
        }

        private void Generate(Step step, bool useStepPosition)
        {
            DecoratorData decoratorData = DecoratorSettings.Decorators[Random.Range(0, DecoratorSettings.Decorators.Count)];

            Decorator decorator = GameObject.Instantiate(decoratorData.DecoratorPrefab, gameObject.transform, true);
            decorator.transform.localScale = Vector3.zero;

            decorator.DecoratorAnimationData = decoratorData.DecoratorAnimationData;

            if (RandomMaterial)
            {
                int randMatID = Random.Range(0, DecoratorSettings.DecoratorMaterials.Count);
                decorator.SetMaterial(DecoratorSettings.DecoratorMaterials[randMatID].BaseMaterial, (DecoratorSettings.DecoratorMaterials[randMatID].SideMaterial));
            }
            else if (step)
            {
                //int matId = step.ColorID - 1;
                //decorator.SetMaterial(PathSettings.StepBaseMaterials[matId], PathSettings.StepSideMaterials[matId]);

                int randMatID = RandomExceptList(DecoratorSettings.DecoratorMaterials.Count, new int[] {step.ColorID - 1});

                if (randMatID < DecoratorSettings.DecoratorMaterials.Count && randMatID >=0 )
                {
                    decorator.SetMaterial(DecoratorSettings.DecoratorMaterials[randMatID].BaseMaterial,
                        (DecoratorSettings.DecoratorMaterials[randMatID].SideMaterial));
                }
            }
            
            if (RemoveLast && decorators.Count > 0)
            {
                Destroy(decorators[decorators.Count - 1].gameObject);
                decorators.RemoveAt(decorators.Count - 1);
            }
            
            decorators.Add(decorator);

            decorator.SetPositionAndRotation(step, useStepPosition);
            decorator.Animate();

            gameEventsSystem?.OnDecoratorCreated?.Invoke(decorator);

        }
        
        // TODO put into library for future purpose
        public static int RandomExceptList(int n, int[] x) 
        {
            System.Random r = new System.Random();
            int result = r.Next(n - x.Length);

            for (int i = 0; i < x.Length; i++) 
            {
                if (result < x[i])
                    return result;
                result++;
            }
            return result;
        }
    }
}