using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class SceneBoundsSystem : BaseInitializableSystem, ITickable
    {
        private GameObject bounds;
        private Collider collider;
        private Camera camera;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            SetupSceneBounds(InitSettings.Camera, 10);
        }

        public bool BoundsReady()
        {
            return (bounds != null);
        }

        public Bounds GetBounds()
        {
            return collider.bounds;
        }
        
        private void SetupSceneBounds(Camera camera, float distance)
        {
            this.camera = camera;
            
            var frustumHeight = 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
            var frustumWidth = frustumHeight * camera.aspect;
            
            bounds = new GameObject("Bounds");
            bounds.transform.localScale = new Vector3(frustumWidth * 1.5f, frustumHeight, 20);
            bounds.tag = "Bounds";
            
            collider = bounds.AddComponent<BoxCollider>();
            collider.isTrigger = true;
            
            bounds.transform.SetParent(camera.transform);
            bounds.transform.localEulerAngles = Vector3.zero;
        }

        public void Tick()
        {
            if (!bounds)
                return;
            
           
        }
    }
}