using System.Collections.Generic;
using System.Linq;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class LevelGenerator : BaseInitializableSystem, ILateInitializable, ITickable
    {
        private PlayerLifeSystem playerLifeSystem;
        private GameEventsSystem gameEventsSystem;
        
        private List<RowGenerator> rowGenerators = new List<RowGenerator>();

        private Transform boundsTransform;

        private float lastGroupOffsetX = 0;

        private bool veryFirstStep = true;

        private int id = 0;

        private float velocity = 0;

        private float playerVelocityFactor = 1;

        private Transform stepsParent;

        private int landedCounter = 0;

        private float newIncrement = 1;

        public LevelGenerator(PlayerLifeSystem playerLifeSystem, GameEventsSystem gameEventsSystem)
        {
            this.playerLifeSystem = playerLifeSystem;
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            stepsParent = new GameObject("Steps Parent").transform;
            stepsParent.transform.position = Vector3.zero;
            
            gameEventsSystem.OnJumpToStep += OnJumpToStep;
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
            gameEventsSystem.OnPlayerMissed += OnPlayerMissed;
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;

            velocity = InitSettings.PathSettings.StepsStartSpeed;

            playerVelocityFactor = 1;
            newIncrement = InitSettings.PathSettings.SpeedIncrement;
        }

        private void OnPlayerLanded(Step arg0, int arg1)
        {
            landedCounter++;

            if (landedCounter == 1)
            {
                //GenerateRows(InitSettings.PathSettings.RowCount, stepsParent);

                for (int i = 1; i < rowGenerators.Count; i++)
                {
                    rowGenerators[i].WakeAll();
                }
            }
        }

        public void LateInitialize()
        {
            GenerateRows(InitSettings.PathSettings.RowCount, stepsParent);

            GenerateBounds(InitSettings.PathSettings.RowCount);
        }
        
        public void Tick()
        {
            for (int i = 0; i < rowGenerators.Count; i++)
            {
                rowGenerators[i].Move(velocity * playerVelocityFactor);
            }

            if (boundsTransform)
            {
                boundsTransform.position = new Vector3(playerLifeSystem.Player.transform.position.x,
                    boundsTransform.position.y,
                    boundsTransform.position.z);
                //+ new Vector3(boundsTransform.localScale.x / 2, 0,0);
            }
        }

        private void OnPlayerDied(DeathReason deathReason)
        {
            if (InitSettings.PathSettings.StopPlatformsWhenDead)
            {
                foreach (var row in rowGenerators)
                {
                    row.StopAll();
                }
            }

            switch (deathReason)
            {
                case DeathReason.Bad_Landed:
                case DeathReason.Not_Landed:
                    playerVelocityFactor = InitSettings.PathSettings.PlayerFallVelocityFactor;
                    break;
                case DeathReason.Out_Of_Screen:
                    playerVelocityFactor = 1;
                    break;
            }
        }

        private void OnJumpToStep(JumpEventArg jumpEventArg)
        {
            rowGenerators[0].DestroyAll(jumpEventArg.JumpFromStep);
            rowGenerators.RemoveAt(0);
            
            GenerateRows(1, stepsParent);

            //velocity += InitSettings.PathSettings.SpeedIncrement;
            if (velocity < InitSettings.PathSettings.MaxVelocity)
            {
                velocity += newIncrement;

                newIncrement *= InitSettings.PathSettings.IncrementMultiplier;
                Debug.Log("new increment " + newIncrement);
                Debug.Log("new velocity " + velocity);
                
                gameEventsSystem.OnVelocityAndIncrement?.Invoke(new VelocityData
                {
                    Velocity = velocity,
                    Increment = newIncrement
                });
            }
            else
            {
                Debug.Log("velocity = " + velocity + " TOO FAST");
            }
        }

        private void GenerateRows(int count, Transform parent)
        {
            for (int i = 0; i < count; i++)
            {
                int c = veryFirstStep ? 1 : InitSettings.PathSettings.CountInRow;
                
                RowGenerator rowGenerator = new RowGenerator(InitSettings, veryFirstStep, parent);
                float posX = i * InitSettings.PathSettings.StepX;
                
                rowGenerator.Generate(posX + lastGroupOffsetX, c, id, landedCounter > 0 || veryFirstStep);
                
                rowGenerators.Add(rowGenerator);

                if (!veryFirstStep)
                {
                    gameEventsSystem.OnRowGenerated?.Invoke(rowGenerator, id);
                }

                if (i == count - 1)
                {
                    lastGroupOffsetX += posX +  InitSettings.PathSettings.StepX;
                }
                
                veryFirstStep = false;

                id++;
            }
        }
        
        private void OnPlayerMissed()
        {
            rowGenerators[0].HideAll();
        }

        private void GenerateBounds(int count)
        {
            GameObject bounds = new GameObject("Bounds");
            bounds.transform.localScale = new Vector3(InitSettings.PathSettings.StepX * count * 2, 5, 
                InitSettings.PathSettings.CountInRow * InitSettings.PathSettings.StepWidthInRow);
            bounds.tag = "Path Bounds";

            boundsTransform = bounds.transform;
            
            Collider collider = bounds.AddComponent<BoxCollider>();
            collider.isTrigger = true;
        }
    }
}