using lab.framework;
using MoreMountains.NiceVibrations;
using IDisposable = System.IDisposable;

namespace sg.staircase
{
    public class HapticSystem: BaseInitializableSystem, IDisposable
    {
        [Inject] private InputSystem inputSystem;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            MMVibrationManager.iOSInitializeHaptics ();
        }

        public void Dispose()
        {
            MMVibrationManager.iOSReleaseHaptics ();
        }
        
    }
}