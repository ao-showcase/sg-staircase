using lab.framework;
using UnityEngine;
using UnityEngine.Events;

namespace sg.staircase
{
    public class InputSystem: BaseInitializableSystem, ITickable, IDisposable
    {
        public UnityAction<float> OnTap;
        
        private float tapStartTime;
        
        public void Tick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                //tapStartTime = Time.unscaledTime;
                
                OnTap?.Invoke(tapStartTime);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                //float tapTime = Time.unscaledTime - tapStartTime;
                //OnTap?.Invoke(tapTime);
            }
        }

        public void Dispose()
        {
            OnTap = null;
        }
    }
}