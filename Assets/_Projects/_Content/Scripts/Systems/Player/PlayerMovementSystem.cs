using System;
using System.Collections;
using DefaultNamespace;
using DG.Tweening;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public enum FallSide
    {
        Left,
        Right
    }
    
    public class PlayerMovementSystem : BaseInitializableSystem, ITickable
    {
        private PlayerLifeSystem playerLifeSystem;
        private InputSystem inputSystem;
        private GameEventsSystem gameEventsSystem;

        public Vector3 CurrentPlayerPosition => player.transform.position;

        private Player player;
        private bool grounded;
        private float maxRaycastDist = 5;

        private int currentStep = 0;
        
        private Sequence curJumpSeq;

        private bool playerAlive = true;
        
        private float curX = 0;

        private Transform curStepTransform;
        
        private Vector3 pivotOffset = new Vector3(0, 0.25f, 0);
        private float raycastDistance = 5;

        private int layer = 1 << 31;

        private Step stepLanded;

        public PlayerMovementSystem(PlayerLifeSystem playerLifeSystem, InputSystem inputSystem, GameEventsSystem gameEventsSystem)
        {
            this.playerLifeSystem = playerLifeSystem;
            this.inputSystem = inputSystem;
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            InitSettings.AsyncProcessor.StartCoroutine(SetupPlayer());
            
            inputSystem.OnTap += OnTap;
            
            gameEventsSystem.OnPlayerDied += delegate { playerAlive = false; };
        }

        public void Tick()
        {
            if (playerAlive && player)
            {
                Vector3 screenPos = InitSettings.Camera.WorldToScreenPoint(player.transform.position);
                
                if (screenPos.x <= 0 || screenPos.x >= Screen.width)
                {
                    playerAlive = false;
                    gameEventsSystem.OnPlayerDied?.Invoke(DeathReason.Out_Of_Screen);
                }

                if (player.transform.position.y < -1)
                {
                    playerAlive = false;
                    gameEventsSystem.OnPlayerDied?.Invoke(DeathReason.Not_Landed);
                    gameEventsSystem.OnPlayerMissed?.Invoke();
                }
                
                Debug.DrawLine(player.transform.position + pivotOffset, player.transform.position + pivotOffset + Vector3.down * raycastDistance, Color.red);
            }
            
            
        }
        
        private IEnumerator SetupPlayer()
        {
            yield return new WaitWhile(() => playerLifeSystem.Player == null);

            player = playerLifeSystem.Player;
            player.Hit.OnTriggerEntered += OnTriggerEntered;
            player.Hit.OnTriggerExited += OnTriggerExited;
        }

        private void OnTap(float tapTime)
        {
            if (!grounded || !playerAlive)
                return;

            player.transform.parent = null;
            
            grounded = false;
            
            player.BonusAdditionalScore = 0;
            
            var toPosition = player.transform.position + new Vector3(InitSettings.PathSettings.StepX, 0, 0);

            if (InitSettings.PlayerSettings.Ease == Ease.INTERNAL_Custom)
            {
                curJumpSeq = player.transform.DOJump(toPosition, InitSettings.PlayerSettings.BaseJumpForce, 1,
                        InitSettings.PlayerSettings.JumpTime, false).SetEase(InitSettings.PlayerSettings.EaseCurve)
                    .SetUpdate(false).OnComplete(OnJumpCompleted);

            }
            else
            {
                curJumpSeq = player.transform.DOJump(toPosition, InitSettings.PlayerSettings.BaseJumpForce, 1,
                        InitSettings.PlayerSettings.JumpTime, false).SetEase(InitSettings.PlayerSettings.Ease)
                    .SetUpdate(false).OnComplete(OnJumpCompleted);
            }
            
            
            player.Rigidbody.isKinematic = false;
            player.DoJump();

            currentStep++;
            gameEventsSystem.OnJumpToStep?.Invoke(new JumpEventArg(currentStep, stepLanded));
            
            
            
        }
        
        private void OnTriggerEntered(Player player, Collider other)
        {
            if (other.gameObject.CompareTag("Step") && !grounded && playerAlive)
            {        
                //Debug.Log("========================================= " + currentStep);
                
                //Debug.Log("Step collision " + playerAlive);
                
                RaycastHit hit;

                if (InitSettings.PathSettings.LandedOnStepSide)
                {
                    LandedOnStep(player, other);
                }
                else
                {
                    if (Physics.Raycast(player.transform.position + pivotOffset, Vector3.down, out hit, raycastDistance,
                            layer) && playerAlive)
                    {
                        if (hit.collider.CompareTag("Step"))
                        {
                            //Debug.Log("Step Raycast");

                            LandedOnStep(player, other);
                        }
                    }
                    else
                    {
                        player.transform.parent = null;

                        playerAlive = false;

                        gameEventsSystem.OnPlayerDied?.Invoke(DeathReason.Bad_Landed);
                        gameEventsSystem.OnPlayerMissed?.Invoke();
                        
                        
                        if (Physics.Raycast(player.transform.position + pivotOffset,
                            new Vector3(0, -3, 3) * raycastDistance, out hit))
                        {
                            DoFall(FallSide.Left);
                        }
                        else if (Physics.Raycast(player.transform.position + pivotOffset,
                            new Vector3(0, -3, -3) * raycastDistance, out hit))
                        {
                            DoFall(FallSide.Right);
                        }
                    }
                }
            }
            
        }
        
        private void DoFall(FallSide fallSide)
        {
            switch (fallSide)
            {
                case FallSide.Left:
                    
                    //player.Rigidbody.AddForce(new Vector3(0, -0.5f, -1f) * InitSettings.PlayerSettings.EdgeFallForce,ForceMode.Impulse);
                    player.Rigidbody.DORotate(new Vector3(-90, 0, 0), InitSettings.PlayerSettings.RotateSideDuration, RotateMode.LocalAxisAdd);
                    player.Rigidbody.DOMoveZ(player.transform.position.z - InitSettings.PlayerSettings.FallSideDistance, InitSettings.PlayerSettings.FallSideDuration);
                    
                    break;
                case FallSide.Right:
                    
                    //player.Rigidbody.AddForce(new Vector3(0, -0.5f, 1f) * InitSettings.PlayerSettings.EdgeFallForce, ForceMode.Impulse);
                    player.Rigidbody.DORotate(new Vector3(90, 0, 0), InitSettings.PlayerSettings.RotateSideDuration, RotateMode.LocalAxisAdd);
                    player.Rigidbody.DOMoveZ(player.transform.position.z + InitSettings.PlayerSettings.FallSideDistance,  InitSettings.PlayerSettings.FallSideDuration);
                    
                    break;
            }
        }

        private void LandedOnStep(Player player, Collider other)
        {
           // Debug.Log("Landed");
            
            grounded = true;

            stepLanded = other.GetComponent<StepHit>().Parent.GetComponent<Step>();
                        
            player.Rigidbody.isKinematic = true;
            ///player.transform.SetParent(other.gameObject.GetComponentInChildren<StepHit>().Parent.PlayerHolder);
            gameEventsSystem.OnPlayerLanded?.Invoke(stepLanded, player.BonusAdditionalScore);
                        
            player.SetParentTransform(stepLanded.transform);
        }
        
        private void OnTriggerExited(Player player, Collider other)
        {
        }

        private void OnJumpCompleted()
        {
        }

    }
}