using lab.framework;

namespace sg.staircase
{
    public class PlayerColorSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        private PlayerLifeSystem playerLifeSystem;

        public PlayerColorSystem(GameEventsSystem gameEventsSystem, PlayerLifeSystem playerLifeSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
            this.playerLifeSystem = playerLifeSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
        }

        private void OnPlayerLanded(Step step, int addScore)
        {
            if (!playerLifeSystem.Player)
                return;

            playerLifeSystem.Player.SetColor(step, step.Color, step.SideColor);
        }
    }
}