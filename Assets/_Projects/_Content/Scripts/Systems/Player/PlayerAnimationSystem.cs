using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class PlayerAnimationSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        private PlayerLifeSystem playerLifeSystem;
        private static readonly int jumpParam = Animator.StringToHash("Jump");
        private static readonly int landedParam = Animator.StringToHash("Landed");
        private static readonly int missedParam = Animator.StringToHash("Missed");

        public PlayerAnimationSystem(GameEventsSystem gameEventsSystem, PlayerLifeSystem playerLifeSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
            this.playerLifeSystem = playerLifeSystem;
        }


        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            gameEventsSystem.OnPlayerMissed += OnPlayerMissed;
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
            gameEventsSystem.OnJumpToStep += OnJumpToStep;
        }

        private void OnPlayerMissed()
        {

            
            playerLifeSystem.Player.Animator.SetTrigger(missedParam);
        }

        private void OnJumpToStep(JumpEventArg jumpEventArg)
        {

            
            playerLifeSystem.Player.Animator.SetTrigger(jumpParam);
        }

        private void OnPlayerLanded(Step step, int addScore)
        {

            
            playerLifeSystem.Player.Animator.SetTrigger(landedParam);
        }
    }
}