using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class PlayerLifeSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;

        public PlayerLifeSystem(GameEventsSystem _gameEventsSystem)
        {
            gameEventsSystem = _gameEventsSystem;
        }
        
        public Player Player
        {
            get { return player; }
        }

        private Player player;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            player = GameObject.FindObjectOfType<Player>();
        }

    }
}