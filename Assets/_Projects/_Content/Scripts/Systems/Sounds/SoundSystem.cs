using System.Collections.Generic;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class SoundSystem : BaseInitializableSystem
    {
        private GameEventsSystem gameEventsSystem;
        
        private List<BaseSoundPlayer> soundPlayers = new List<BaseSoundPlayer>();
        private DontDestroySounds dontDestroySounds;

        private int curStep = 0;

        public SoundSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            GetOrCreateDontDestroySounds();
            
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
            gameEventsSystem.OnDecoratorCreated += OnDecoratorCreated;
            gameEventsSystem.OnJumpToStep += OnJumpToStep;
            gameEventsSystem.OnBonusCatched += OnBonusCatched;
            gameEventsSystem.OnComboDataIncreased += OnComboDataIncreased;
            
//            FxRegistrator.OnSoundPlayerStarted += OnSoundPlayerStarted;
//            FxRegistrator.OnSoundPlayerDestroyed += OnSoundPlayerDestroyed;
        }      


        private void GetOrCreateDontDestroySounds()
        {
            dontDestroySounds = GameObject.FindObjectOfType<DontDestroySounds>();

            if (!dontDestroySounds)
            {
                dontDestroySounds = GameObject.Instantiate(InitSettings.SoundSettings.DontDestroySounds);
            }

            foreach (var soundPlayer in dontDestroySounds.GetComponentsInChildren<BaseSoundPlayer>())
            {
                OnSoundPlayerStarted(soundPlayer);
                soundPlayer.Init();
            }
        }

        private void OnSoundPlayerStarted(BaseSoundPlayer soundPlayer)
        {
            soundPlayers.Add(soundPlayer);
        }
        
        private void OnSoundPlayerDestroyed(BaseSoundPlayer soundPlayer)
        {
            soundPlayers.Remove(soundPlayer);
        }

        private void OnPlayerLanded(Step step, int id)
        {
            curStep++;
            SetStep();
            PlayOnEvent(AnimateEvent.Land, step, false);
        }
        
        private void OnPlayerDied(DeathReason reason)
        {
            PlayOnEvent(AnimateEvent.Death, null, false);
        }
        
        private void OnDecoratorCreated(Decorator decorator)
        {
            PlayOnEvent(AnimateEvent.DecoratorCreated, null, false);
        }

        private void OnJumpToStep(JumpEventArg arg0)
        {
            PlayOnEvent(AnimateEvent.Jump, null, false);
        }
        
        private void OnBonusCatched(Bonus arg0)
        {
            PlayOnEvent(AnimateEvent.BonusCatched, null, false);
        }
        
        private void OnComboDataIncreased(ComboData comboData)
        {
            if (comboData.Counter != 0)
            {
                PlayOnEvent(AnimateEvent.Combo, null, true);
            }
        }

        private void SetStep()
        {
            foreach (var soundPlayer in soundPlayers)
            {
                soundPlayer.SetStep(curStep);
            }
        }
        
        private void PlayOnEvent(AnimateEvent animateEvent, Step step, bool playNote)
        {
            foreach (var soundPlayer in soundPlayers)
            {
                if (soundPlayer.AnimateEvent == animateEvent)
                {
                    if (soundPlayer.RandomSound)
                    {
                        soundPlayer.Play();
                    }
                    else if (playNote)
                    {
                        int clipNum = Random.Range(0, soundPlayer.audioClips.Count);
                        soundPlayer.Play(clipNum);
                    }
                    else if (step)
                    {
                        soundPlayer.Play(step.ColorID);
                    }
                }
            }
        }
    }
}