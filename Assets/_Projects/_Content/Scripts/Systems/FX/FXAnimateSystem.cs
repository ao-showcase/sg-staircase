using System.Collections.Generic;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class FXAnimateSystem : BaseInitializableSystem, IDisposable
    {
        private GameEventsSystem gameEventsSystem;
        private InputSystem inputSystem;
        
        private List<BaseAnimateOnEvent> animateOnEvents = new List<BaseAnimateOnEvent>();

        private bool firstLanded = true;
        private bool firstJump = true;

        private bool playerAlive = true;
        private bool reloading = false;

        public FXAnimateSystem(GameEventsSystem gameEventsSystem, InputSystem inputSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
            this.inputSystem = inputSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            FxRegistrator.OnAnimatedObjectStarted += OnAnimatedObjectStarted;
            FxRegistrator.OnAnimatedObjectDestroyed += OnAnimatedObjectDestroyed;
            
            gameEventsSystem.OnJumpToStep += OnJumpToStep;
            gameEventsSystem.OnPlayerLanded += OnPlayerLanded;
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
            
            gameEventsSystem.OnComboDataIncreased += OnComboDataIncreased;
            gameEventsSystem.OnComboFailed += OnComboFailed;
            
            inputSystem.OnTap += OnTap;
        }

        private void OnTap(float arg0)
        {
            if (!playerAlive && !reloading)
            {
                reloading = true;
                RunAnim(AnimateEvent.TapOnDeathScreen, null);
            }
        }

        private void OnPlayerDied(DeathReason reason)
        {
            playerAlive = false;
            
            RunAnim(AnimateEvent.Death, null);
            RunAnim(AnimateEvent.Combo, new ComboAnimateData(false, null));
        }

        private void OnPlayerLanded(Step step, int s)
        {
            if (firstLanded)
            {
                firstLanded = false;
                return;
            }
            
            RunAnim(AnimateEvent.Land, new IntAnimateData(step.ColorID));
        }

        private void OnJumpToStep(JumpEventArg jumpArg)
        {
            if (firstJump)
            {
                firstJump = false;
                RunAnim(AnimateEvent.FirstJump, null);
            }
            
            RunAnim(AnimateEvent.Jump, null);
        }
        
        private void OnComboDataIncreased(ComboData comboData)
        {
            RunAnim(AnimateEvent.Combo, new ComboAnimateData(true, comboData));
        }
        
        private void OnComboFailed()
        {
            RunAnim(AnimateEvent.Combo, new ComboAnimateData(false, null));
        }

        private void RunAnim(AnimateEvent animateEvent, IAnimateData animateData)
        {
            for (int i = animateOnEvents.Count - 1; i >= 0; i--)
            {
                if (animateOnEvents[i] == null)
                {
                    animateOnEvents.RemoveAt(i);
                }
                else
                {
                    animateOnEvents[i].RunAnimation(animateEvent, animateData);
                }
            }
        }

        private void OnAnimatedObjectStarted(BaseAnimateOnEvent baseAnimateOnEvent)
        {
            animateOnEvents.Add(baseAnimateOnEvent);
        }

        private void OnAnimatedObjectDestroyed(BaseAnimateOnEvent baseAnimateOnEvent)
        {
            animateOnEvents.Remove(baseAnimateOnEvent);
        }
        
        public void Dispose()
        {
            FxRegistrator.Dispose();
        }
    }
}