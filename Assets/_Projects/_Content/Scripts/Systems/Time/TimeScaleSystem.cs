using DG.Tweening;
using lab.framework;
using UnityEngine;

namespace sg.staircase
{
    public class TimeScaleSystem : BaseInitializableSystem, IDisposable
    {
        private GameEventsSystem gameEventsSystem;
        
        Sequence sequence = DOTween.Sequence();

        public TimeScaleSystem(GameEventsSystem gameEventsSystem)
        {
            this.gameEventsSystem = gameEventsSystem;
        }

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            Time.timeScale = 1;
            
            gameEventsSystem.OnPlayerDied += OnPlayerDied;
        }
        
        public void Dispose()
        {            
            sequence.Kill();
            Time.timeScale = 1;
            DOTween.KillAll();
        }

        private void OnPlayerDied(DeathReason deathReason)
        {
            if (!InitSettings.TimeSettings.UserDeathTimeScale)
                return;
            
            sequence.Append(DOTween.To(ChangeTime, InitSettings.TimeSettings.slowdownValueDeath, 1,
                    InitSettings.TimeSettings.slowdownDurationDeath)
                .SetUpdate(true)
                .SetEase(InitSettings.TimeSettings.timeScaleOutCurveDeath));
        }
        
        private void ChangeTime(float val)
        {
            Time.timeScale = val;
            Time.fixedDeltaTime = val * 0.02f;
        }
    }
}