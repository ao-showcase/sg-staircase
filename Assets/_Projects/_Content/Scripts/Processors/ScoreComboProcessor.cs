using UnityEngine;

namespace sg.staircase
{
    public class ComboData
    {
        public int Score;
        public int Counter;
        public Color Color;

        public ComboData(int score, int counter, Color color)
        {
            Score = score;
            Counter = counter;
            Color = color;
        }
    }
    
    public class ScoreComboProcessor
    {
        private Step prevStep;
        private int sameCounter = 0;
        
        public ComboData ComboBonus(Step step, int comboMultiplier)
        {
            if (!prevStep)
            {
                //Debug.Log("--- no prev step");
                
                prevStep = step;
                return new ComboData(1, 0, step.Color);
            }
            else
            {
                if (prevStep.ColorID == step.ColorID)
                {
                    prevStep = step;
                
                    sameCounter++;
                    
                    //Debug.Log("--- sameCounter " + sameCounter + " " + (sameCounter * comboMultiplier));
                    return new ComboData(sameCounter * comboMultiplier, sameCounter, step.Color);
                }
                else
                {
                    //Debug.Log("--- no prev ");
                    
                    prevStep = step;
                    sameCounter = 0;
                    return new ComboData(1, 0, step.Color);
                }
            }
        }
    }
}