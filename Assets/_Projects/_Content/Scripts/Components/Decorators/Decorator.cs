using DG.Tweening;
using UnityEngine;

namespace sg.staircase
{
    public class Decorator : MonoBehaviour
    {
        public DecoratorAnimationData DecoratorAnimationData;

        private Renderer decoratorRenderer;

        private void Awake()
        {
            decoratorRenderer = GetComponentInChildren<Renderer>();
        }

        public void SetPositionAndRotation(Step step, bool useStepPosition)
        {
            float randY = Random.Range( DecoratorAnimationData.HeightRange.x, DecoratorAnimationData.HeightRange.y );
            float randZ = Random.Range( DecoratorAnimationData.ZOffsetRange.x, DecoratorAnimationData.ZOffsetRange.y );

            if (!useStepPosition)
            {
                transform.position = new Vector3(transform.position.x, randY, transform.position.z);

            }
            else
            {
                transform.position = new Vector3(step.transform.position.x, randY, step.transform.position.z + randZ);
            }

            float randRot = Random.Range( DecoratorAnimationData.RotationRange.x, DecoratorAnimationData.RotationRange.y );
            
            transform.eulerAngles = new Vector3(0, randRot, 0);
        }
        
        public void Animate()
        {
            AnimationCurve randCurve = DecoratorAnimationData.AnimationCurves[Random.Range(0, DecoratorAnimationData.AnimationCurves.Count)];

            float randScaleTo = Random.Range(DecoratorAnimationData.ScaleToRange.x, DecoratorAnimationData.ScaleToRange.y); 
            float randDuration = Random.Range(DecoratorAnimationData.DurationRange.x, DecoratorAnimationData.DurationRange.y); 
            
            transform.DOScale(randScaleTo, randDuration).SetUpdate(true).SetEase(randCurve);
        }

        public void SetMaterial(Material baseMat, Material sideMat)
        {
            if (!decoratorRenderer)
            {
                decoratorRenderer = GetComponentInChildren<Renderer>();
            }
            
            Material[] mats = decoratorRenderer.materials;

            mats[0] = baseMat;          
            mats[1] = sideMat;          

            decoratorRenderer.materials = mats;
        }
    }
}