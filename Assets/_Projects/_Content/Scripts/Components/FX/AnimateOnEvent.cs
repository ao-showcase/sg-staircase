using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace sg.staircase
{
   
    public class AnimateOnEvent : BaseAnimateOnEvent
    {
        public List<AnimateEventData> AnimateEvents = new List<AnimateEventData>();
        public Animator Animator;
        
        public override void RunAnimation(AnimateEvent animateEvent, IAnimateData animateData)
        {
            if (!Animator)
                return;

            foreach (var anim in AnimateEvents)
            {
                if (anim.AnimateEvent == animateEvent)
                {
                    if (anim.AnimationType == AnimationType.Bool)
                    {
                        Animator.SetBool(anim.ParamName, anim.BoolValue);

                        //Debug.Log("Anim bool: " + anim.ParamName + " " + anim.BoolValue + " " + animateEvent);
                    }
                    else if (anim.AnimationType == AnimationType.Trigger)
                    {
                        Animator.SetTrigger(anim.ParamName);
                        
                        //Debug.Log("Anim trigger: " + anim.ParamName + " " + " " + animateEvent);
                    }
                    else if (anim.AnimationType == AnimationType.Int)
                    {
                        IntAnimateData intAnimateData = animateData as IntAnimateData;
                        
                        Animator.SetInteger(anim.ParamName, intAnimateData.Value);
                        
                       //Debug.Log("Anim int: " + anim.ParamName + " " + intAnimateData.Value + " " + animateEvent);
                    }
                }
            }
        }
    }
}