using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace sg.staircase
{
    public class CameraSizeTweenOnEvent : BaseAnimateOnEvent
    {
        [Title("Камера")]
        public Camera Camera;
        
        [Title("Настройки возврата в изначальное состояние")]
        public AnimationCurve TweenOutCurve;
        public float TweenOutTime;
        
        [Title("Список анимаций для разных Combo")]
        public List<CameraSizeTweenAnimationData> AnimateEvents = new List<CameraSizeTweenAnimationData>();
        
        private int maxComboID = 0;
        private float initSizeValue;

        private void Awake()
        {
            foreach (var anim in AnimateEvents)
            {
                if (anim.ComboCount > maxComboID)
                {
                    maxComboID = anim.ComboCount;
                }
            }
            
            initSizeValue = Camera.orthographicSize;
        }

        public override void RunAnimation(AnimateEvent animateEvent, IAnimateData animateData)
        {
            if (!Camera)
                return;
            
            foreach (var anim in AnimateEvents)
            {
                if (anim.AnimateEvent == animateEvent)
                {
                    if (anim.AnimationType == CameraSizeTweenAnimationType.CameraSizeOnce)
                    {
                        Camera.DOOrthoSize(anim.toVal, anim.time).
                            SetEase(anim.tweenInCurve);
                    }
                    else if (anim.AnimationType == CameraSizeTweenAnimationType.CameraSizeCombo)
                    {
                        ComboAnimateData comboAnimateData = (ComboAnimateData) animateData;

                        if (comboAnimateData.ComboSuccess && comboAnimateData.ComboData.Counter > maxComboID)
                        {
                            comboAnimateData.ComboData.Counter = maxComboID;
                        }

                        if (comboAnimateData.ComboSuccess && anim.ComboCount == comboAnimateData.ComboData.Counter)
                        {
                            Camera.DOOrthoSize(anim.toVal, anim.time).
                                SetEase(anim.tweenInCurve);
                        }
                        else if (!comboAnimateData.ComboSuccess)
                        {
                            Camera.DOOrthoSize(initSizeValue, TweenOutTime).SetEase(TweenOutCurve);
                        }
                    }
                }
            }
        }

    }
}