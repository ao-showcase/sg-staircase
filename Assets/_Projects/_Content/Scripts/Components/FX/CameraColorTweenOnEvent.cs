using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

namespace sg.staircase
{
    public class CameraColorTweenOnEvent : BaseAnimateOnEvent
    {
        [Title("Камера")]
        public Camera Camera;
        
        [Title("Настройки возврата в изначальное состояние")]
        public AnimationCurve TweenOutCurve;
        public float TweenOutTime;
        
        [Title("Список анимаций для разных Combo")]
        public List<CameraSizeTweenAnimationData> AnimateEvents = new List<CameraSizeTweenAnimationData>();
        
        private int maxComboID = 0;
        private Color initColorValue;

        private void Awake()
        {
            foreach (var anim in AnimateEvents)
            {
                if (anim.ComboCount > maxComboID)
                {
                    maxComboID = anim.ComboCount;
                }
            }
            
            initColorValue = Camera.backgroundColor;
        }

        public override void RunAnimation(AnimateEvent animateEvent, IAnimateData animateData)
        {
            if (!Camera)
                return;
            
            foreach (var anim in AnimateEvents)
            {
                if (anim.AnimateEvent == animateEvent)
                {
                    if (anim.AnimationType == CameraSizeTweenAnimationType.CameraColorCombo)
                    {
                        ComboAnimateData comboAnimateData = (ComboAnimateData) animateData;

                        if (comboAnimateData.ComboSuccess && comboAnimateData.ComboData.Counter > maxComboID)
                        {
                            comboAnimateData.ComboData.Counter = maxComboID;
                        }

                        if (comboAnimateData.ComboSuccess && anim.ComboCount == comboAnimateData.ComboData.Counter)
                        {
                            Color toColor = comboAnimateData.ComboData.Color;
                            
                            //Debug.Log("color of platform = " + toColor);
                            
                            float h, s, v;
                            Color.RGBToHSV(toColor, out h, out s, out v);
                            toColor = Color.HSVToRGB(h, anim.toVal, v);

                            Debug.Log("toVal = " + anim.toVal);
                            Debug.Log("to color = " + toColor + " h " + h + " s " + s + " v " + v);
                            
                            Camera.DOColor(toColor, anim.time).
                                SetEase(anim.tweenInCurve);
                        }
                        else if (!comboAnimateData.ComboSuccess)
                        {
                            Camera.DOColor(initColorValue, TweenOutTime).SetEase(TweenOutCurve);
                        }
                    }
                }
            }
        }
    }
}