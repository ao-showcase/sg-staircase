using System.Collections;
using UnityEngine;

namespace sg.staircase
{
    public abstract class BaseAnimateOnEvent : MonoBehaviour
    {
        private IEnumerator Start()
        {            
            yield return new WaitForEndOfFrame();
            FxRegistrator.OnAnimatedObjectStarted?.Invoke(this);
        }

        private void OnDestroy()
        {
            FxRegistrator.OnAnimatedObjectDestroyed?.Invoke(this);
        }

        public abstract void RunAnimation(AnimateEvent animateEvent, IAnimateData animateData);
    }
}