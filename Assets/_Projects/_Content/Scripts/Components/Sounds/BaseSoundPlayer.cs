using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace sg.staircase
{
    [RequireComponent(typeof(AudioSource))]
    public abstract class BaseSoundPlayer : MonoBehaviour
    {
        public AnimateEvent AnimateEvent;
        public bool RandomSound = false;
        public int EnableSinceStepWithNumber = 0;
        protected bool volumeEnabled = false;
        
        private float volumeOn = 0;
        public float FadeDuration = 0.1f;
        
        public AnimationCurve VolumeCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        
        public List<AudioClip> audioClips = new List<AudioClip>();
        protected AudioSource audioSource;

        protected virtual void Awake()
        {
            audioSource = GetComponent<AudioSource>();

            volumeOn = audioSource.volume;
            
            if (EnableSinceStepWithNumber > 0)
            {
                audioSource.volume = 0;
                volumeEnabled = false;
            }
        }

        public virtual void Init()
        {
            if (EnableSinceStepWithNumber > 0)
            {
                DisableVolume();
            }
        }
        
        protected virtual IEnumerator Start()
        {            
            yield return new WaitForEndOfFrame();
            //FxRegistrator.OnSoundPlayerStarted?.Invoke(this);
        }

        private void OnDestroy()
        {
            //FxRegistrator.OnSoundPlayerDestroyed?.Invoke(this);
        }

        public abstract void Play();
        public abstract void Play(int note);

        public void SetStep(int step)
        {
            if (step >= EnableSinceStepWithNumber && !volumeEnabled)
            {
                EnableVolume();
            }
        }
        
        private void EnableVolume()
        {
            volumeEnabled = true;
            audioSource.DOFade(volumeOn, FadeDuration).SetUpdate(true).SetEase(VolumeCurve);
        }

        private void DisableVolume()
        {
            volumeEnabled = false; 
            audioSource.DOFade(0, FadeDuration).SetUpdate(true).SetEase(VolumeCurve);
        }
    }
}