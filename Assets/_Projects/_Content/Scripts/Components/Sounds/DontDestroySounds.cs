using UnityEngine;

namespace sg.staircase
{
    public class DontDestroySounds : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}