using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sg.staircase
{
    public class EventSoundsPlayer : BaseSoundPlayer
    {
        
        public override void Play()
        {
            //Debug.Log("PLAY!!!");
            if (RandomSound)
            {
                
                int clipNum = Random.Range(0, audioClips.Count);
                
                audioSource.PlayOneShot(audioClips[clipNum]);
            }
        }

        public override void Play(int note)
        {
            //Debug.Log("PLAY 2!!!");
            
            if (!RandomSound && note < audioClips.Count)
            {
               
                
                audioSource.PlayOneShot(audioClips[note]);
            }
        }


    }
}