using DG.Tweening;
using UnityEngine;

namespace sg.staircase
{
    public class Player : MonoBehaviour
    {
        public LayerMask StepLayerMask;
        public Animator Animator;

        [SerializeField] private float colorDuration = 0.1f;
        [SerializeField] private AnimationCurve easeCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));
        
        [HideInInspector] public int BonusAdditionalScore = 0; 
        
        [HideInInspector] public Rigidbody Rigidbody;
        [HideInInspector] public PlayerHit Hit;

        private Transform parent;
        [SerializeField] private bool Coloring;
        [SerializeField] private Renderer Renderer;

        private Color initBaseColor;
        private Color initSideColor;
        
        private float fixedScale = 1;
        
        private Vector3 landOffset = Vector3.zero;
        
        private void Awake()
        {
            SetupLinks();
            GetInitColors();
        }

        private void OnDestroy()
        {
            SetInitColors();
        }

        private void Update()
        {
            if (!parent)
            {
                transform.localScale = Vector3.one;
            }
            else
            {
                transform.position = parent.position + landOffset;
            }
        }

        private void SetupLinks()
        {
            Rigidbody = GetComponent<Rigidbody>();
            Hit = GetComponentInChildren<PlayerHit>();
            Hit.Parent = this;
        }

        private void SetInitColors()
        {
            if (!Renderer)
                return;

            Renderer.materials[0].SetColor("_BaseColor", initBaseColor);
            Renderer.materials[1].SetColor("_BaseColor", initSideColor);
        }
        
        private void GetInitColors()
        {
            if (!Renderer)
                return;

            initBaseColor = Renderer.materials[0].GetColor("_BaseColor");
            initSideColor = Renderer.materials[1].GetColor("_BaseColor");
        }

        public void SetParentTransform(Transform parent)
        {
            this.parent = parent;
            landOffset = transform.position - parent.position;
        }

        public void DoJump()
        {
            this.parent = null;
        }

        public void SetColor(Step step, Color32 stepColor, Color32 stepSideColor)
        {            
            if (Coloring && Renderer && !step.FirstStep)
            {
                //Renderer.materials[0].SetColor("_BaseColor", stepColor);
                //Renderer.materials[1].SetColor("_BaseColor", stepSideColor);
                
                Renderer.materials[0].DOColor(step.StepColorForPlayer.BaseColor, "_BaseColor",colorDuration).SetEase(easeCurve);
                Renderer.materials[1].DOColor(step.StepColorForPlayer.SideColor, "_BaseColor",colorDuration).SetEase(easeCurve);
            }
        }
    }
}