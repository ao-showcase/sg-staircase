using System.Collections;
using TMPro;
using UnityEngine;

namespace sg.staircase
{
    public class UIScoreInScene : MonoBehaviour
    {
        [SerializeField] private bool sameColorAsStep = false;
        
        private TextMeshProUGUI scoreText;
        private float hideDelay = 0;
        private Animator animator;

        private void Awake()
        {
            scoreText = GetComponentInChildren<TextMeshProUGUI>();
            animator = GetComponentInChildren<Animator>();
            
        }

        public void Init(int score, float hideDelay, Color32 color)
        {
            this.hideDelay = hideDelay;
            scoreText.SetText("+" + score);

            if (sameColorAsStep)
            {
                //Debug.Log("SetColor " + color);
                SetColor(color);
            }
            
            StartCoroutine(DelayedHide());
        }

        private IEnumerator DelayedHide()
        {
            yield return new WaitForSeconds(hideDelay);
            animator.SetTrigger("Hide");
        }

        private void SetColor(Color32 color)
        {
            scoreText.faceColor = color;
        }
    }
}