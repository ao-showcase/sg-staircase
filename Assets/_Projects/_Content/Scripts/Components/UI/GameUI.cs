using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace sg.staircase
{
    public class GameUI : MonoBehaviour
    {
        public List<Button> RestartButtons;
        public Animator StatesAnimator;

        public List<TextMeshProUGUI> Score;
        
        public List<TextMeshPro> BestScore;
        public List<TextMeshProUGUI> BestScoreUGUI;
    }
}