using UnityEngine;
using UnityEngine.Analytics;

namespace sg.staircase
{
    public class GameAnalytics : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(this);
            
            AnalyticsEvent.GameStart();
        }
    }
}