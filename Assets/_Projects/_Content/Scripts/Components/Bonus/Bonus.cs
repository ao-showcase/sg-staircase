using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace sg.staircase
{
    public class Bonus : MonoBehaviour
    {
        public int BonusScore = 1;
        public float HideTimer = 0.1f;
        [HideInInspector] public bool Catched = false;
        private Animator animator;
        
        [HideInInspector]
        public List<IBonusEffector> Effectors = new List<IBonusEffector>();

        private void Awake()
        {
            animator = GetComponentInChildren<Animator>();
            Effectors = GetComponentsInChildren<IBonusEffector>().ToList();
        }
        
        public void Hit()
        {
            if (!animator)
                return;

            transform.parent = null;
            
            animator.SetTrigger("Hit");

            StartCoroutine(Hide());
        }

        IEnumerator Hide()
        {           
            yield return new WaitForSeconds(HideTimer);
            animator.SetTrigger("Hide");
        }
    }
}