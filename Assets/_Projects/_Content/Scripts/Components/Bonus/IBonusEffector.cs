namespace sg.staircase
{
    public interface IBonusEffector
    {
        void Run(InitSettings initSettings);
        void Stop();
    }
}