using DG.Tweening;
using UnityEngine;

namespace sg.staircase
{
    public class BonusTimeEffector : MonoBehaviour, IBonusEffector
    {
        private Sequence sequence;
        
        public void Run(InitSettings initSettings)
        {
            Debug.Log("Run bonus time effector");
            
            sequence = DOTween.Sequence();
            
            sequence.Append(DOTween.To(ChangeTime, 1, initSettings.TimeSettings.slowdownValue, initSettings.TimeSettings.slowdownDuration)
                .SetUpdate(true)
                .SetEase(initSettings.TimeSettings.timeScaleInCurve)
                .OnComplete( () => DOTween.To(ChangeTime, initSettings.TimeSettings.slowdownValue, 1, initSettings.TimeSettings.slowdownDuration)
                    .SetUpdate(true)
                    .SetEase(initSettings.TimeSettings.timeScaleOutCurve)));
        }

        public void Stop()
        {
        }

        private void ChangeTime(float val)
        {
            Time.timeScale = val;
            Time.fixedDeltaTime = val * 0.02f;
        }
    }
}