using UnityEngine;

namespace sg.staircase
{
    public class FollowCamera : MonoBehaviour
    {
        public GameObject objectToFollow;
    
        public float speed = 2.0f;

        public bool autoOffset = false;
        public Vector3 offset;

        public bool followX;
        public bool followY;
        public bool followZ;

        private void Start()
        {
            if (autoOffset)
            {
                offset = objectToFollow.transform.position - transform.position;
            }
        }
        
        private void Update ()
        {
            if (!objectToFollow)
                return;
            
            float interpolation = speed * Time.deltaTime;
        
            Vector3 position = this.transform.position;
            
            if (followY)
                position.y = Mathf.Lerp(this.transform.position.y, objectToFollow.transform.position.y - offset.y, interpolation);
            
            if (followX)
                position.x = Mathf.Lerp(this.transform.position.x, objectToFollow.transform.position.x - offset.x, interpolation);
            
            if (followZ)
                position.z = Mathf.Lerp(this.transform.position.z, objectToFollow.transform.position.z - offset.z, interpolation);
        
            this.transform.position = position;
        }
    }
}