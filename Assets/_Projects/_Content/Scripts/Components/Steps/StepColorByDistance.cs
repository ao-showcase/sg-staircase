using sg.staircase;
using UnityEngine;

namespace DefaultNamespace
{
    public class StepColorByDistance : MonoBehaviour
    {
        private Material material;

        private Player player;

        private void Awake()
        {
            player = FindObjectOfType<Player>();

            Renderer renderer = GetComponentInChildren<Renderer>();
            material = renderer.sharedMaterial;

        }

        private void Update()
        {
            float dist = Vector3.Distance(player.transform.position, transform.position);

            material.color = Color.Lerp(Color.red, Color.blue, Time.deltaTime * dist);
        }
    }
}