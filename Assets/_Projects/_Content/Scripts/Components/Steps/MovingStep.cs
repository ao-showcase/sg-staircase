using sg;
using UnityEngine;

namespace DefaultNamespace
{
    public class MovingStep : MonoBehaviour
    {
        [MinMaxSlider(0, 100)] public Vector2 SpeedRange;
        [MinMaxSlider(-100, 100)] public Vector2 DistanceRange;

        public GameObject PlayerHolder;

        private float speed;
        private float distance;

        private bool isStatic;
        
        public void Setup(bool isStatic)
        {
            this.isStatic = isStatic;
            
            speed = Random.Range(SpeedRange.x, SpeedRange.y);
            distance = Random.Range(DistanceRange.x, DistanceRange.y);
        }

        private void Update()
        {
            if (isStatic)
            {
                return;
            }
            
            Vector3 newPosition = transform.position;
            newPosition.z += Mathf.Cos(Time.time * speed) * Time.deltaTime * distance; 
            transform.position = newPosition ;
        }
    }
}