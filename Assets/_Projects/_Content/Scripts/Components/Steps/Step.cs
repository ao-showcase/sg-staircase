using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace sg.staircase
{
    public class Step : MonoBehaviour
    {
        public Transform PlayerHolder;
        [HideInInspector] public Rigidbody Rigidbody;
        public Animator Animator;
        public Transform BonusHolder;
        public Transform Pivot;

        [HideInInspector] public StepColorForPlayer StepColorForPlayer;
        
        [HideInInspector] public bool PlayerOnStep = false;
        [HideInInspector] public int ColorID = 0;
        [HideInInspector] public Color32 Color;
        [HideInInspector] public Color32 SideColor;
        
        [HideInInspector] public bool FirstStep = false;
        
        private float fallFinishY = -0.5f;
        private float fallDuration = 0.25f;
        private float riseDuration = 0.25f;

        public void DoAnimationHit()
        {
            Rigidbody.DOMoveY(fallFinishY, fallDuration, false).OnComplete(ReturnToIdle);
        }
        
        private void ReturnToIdle()
        {
            Rigidbody.DOMoveY(0, riseDuration, false);
        }
    }
}