using UnityEngine;
using UnityEngine.Events;

namespace sg.staircase
{
    public class Hit<T> : MonoBehaviour
    {
        public T Parent;

        public UnityAction<T, Collision> OnCollisionEntered;
        public UnityAction<T, Collision> OnCollisionExited;
        public UnityAction<T, Collider> OnTriggerEntered;
        public UnityAction<T, Collider> OnTriggerExited;
        
        protected virtual void OnCollisionEnter(Collision other)
        {
            //Debug.Log("on collision enter " + name + " " + other.gameObject.name);
            
            OnCollisionEntered?.Invoke(Parent, other);
        }

        protected virtual void OnCollisionExit(Collision other)
        {
            //Debug.Log("on collision exit " + name + " " + other.gameObject.name);
            
            OnCollisionExited?.Invoke(Parent, other);
        }

        protected virtual void OnTriggerEnter(Collider other)
        {           
            //Debug.Log("on trigger enter " + name + " " + other.gameObject.name);
            
            OnTriggerEntered?.Invoke(Parent, other);
        }

        protected virtual void OnTriggerExit(Collider other)
        {
            //Debug.Log("on trigger exit " + name + " " + other.gameObject.name);
            
            OnTriggerExited?.Invoke(Parent, other);
        }
    }
}