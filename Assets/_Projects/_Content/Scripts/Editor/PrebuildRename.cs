using UnityEditor;
using System.IO;
using System;

namespace sg.build
{
    public static class PrebuildRename
    {
        private static string standardPackageName = "com.sacrificial.forgo";
        private static string standardProductName = "sg-forgo";

        private static string GetAutoPostfix()
        {
            return DateTime.Now.Date.Day + "" + DateTime.Now.Date.Month + "" + DateTime.Now.Date.Year + "_"  + DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
        }

        private static string GetManualFilename(string path)
        {
            return Path.GetFileNameWithoutExtension(path);
        }
        
        [MenuItem("SGTools/Build/APK Normal Build")]
        public static void BuildAPKNormal ()
        {
            PlayerSettings.applicationIdentifier = standardPackageName;

            PlayerSettings.productName = standardProductName;

            string path = EditorUtility.SaveFolderPanel("Choose the High Place of Sacrifice", "", "");

            BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, path + "/sg-forgo.apk", BuildTarget.Android, BuildOptions.None);
        }
        
        [MenuItem("SGTools/Build/APK Build (manual full rename)")]
        public static void BuildAPKFullRenameManual ()
        {
            string fullPath = EditorUtility.SaveFilePanel("Choose the High Place of Sacrifice", "", "", "apk");
            
            string filename = GetManualFilename(fullPath);
            string folder = Path.GetDirectoryName(fullPath);
            
            PlayerSettings.applicationIdentifier = standardPackageName + "_" + GetAutoPostfix();
            PlayerSettings.productName = filename;
            
            BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, folder + "/" + filename + ".apk", BuildTarget.Android, BuildOptions.None);
        }
        
        [MenuItem("SGTools/Build/APK Build (auto full rename)")]
        public static void BuildAPKFullRenameAuto ()
        {
            string postfix = GetAutoPostfix();
            
            PlayerSettings.applicationIdentifier = standardPackageName + "_" + postfix;
            PlayerSettings.productName = postfix + "_" + standardProductName;

            string path = EditorUtility.SaveFolderPanel("Choose the High Place of Sacrifice", "", "");           
            //string path = EditorUtility.SaveFilePanel("Choose the High Place of Sacrifice", "", "", ".apk");           
            
            BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, path + "/sg-forgo_" + postfix + ".apk", BuildTarget.Android, BuildOptions.None);
        }
        
        [MenuItem("SGTools/Build/APK Build (auto package and product rename)")]
        public static void BuildAPKPackageRenameAuto ()
        {
            string postfix = GetAutoPostfix();
            
            PlayerSettings.applicationIdentifier = standardPackageName + "_" + postfix;
            PlayerSettings.productName = postfix + "_" + standardProductName;

            string path = EditorUtility.SaveFolderPanel("Choose the High Place of Sacrifice", "", "");

            BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, path + "/sg-forgo.apk", BuildTarget.Android, BuildOptions.None);
        }
    }

}