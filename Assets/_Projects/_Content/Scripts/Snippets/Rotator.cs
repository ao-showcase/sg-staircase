﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    float xRot;
    float yRot;
    float zRot;

    public float xRotationSpeed;
    public float yRotationSpeed;
    public float zRotationSpeed;

    void Update()
    {
        xRot += Time.deltaTime * xRotationSpeed;
        yRot += Time.deltaTime * yRotationSpeed;
        zRot += Time.deltaTime * zRotationSpeed;

        transform.localRotation = Quaternion.Euler(xRot, yRot, zRot);
    }
}