﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Offsetter : MonoBehaviour
{
    // Scroll main texture based on time

    public float xScrollSpeed = 0.0f;
    public float yScrollSpeed = -0.1f;
    Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        float xOffset = Time.time * xScrollSpeed;
        float yOffset = Time.time * yScrollSpeed;
        rend.material.SetTextureOffset("_MainTex", new Vector2(xOffset, yOffset));
    }
}