﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    [SerializeField] private Transform childObjectToFlip;
    
    private Camera cameraToLookAt;
    // Start is called before the first frame update
    void Start ()
    {
        cameraToLookAt = Camera.main;
    }

    // Update is called once per frame
    void Update ()
    {
        Vector3 lookatPos = 2 * transform.position - cameraToLookAt.transform.position;
        transform.LookAt (lookatPos, cameraToLookAt.transform.rotation * Vector3.up);
    }
}
