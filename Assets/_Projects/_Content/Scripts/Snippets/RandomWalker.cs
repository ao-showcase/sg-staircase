﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomWalker : MonoBehaviour
{

    public float mul = 1f;
    public float speed = 1f;

    Vector3 acc = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float x = transform.localPosition.x;
        float y = transform.localPosition.y;

        Vector3 target = 
            Vector3.right * Random.Range(-x + 0.1f, x + 0.1f) +
            Vector3.up    * Random.Range(-y + 0.1f, y + 0.1f);
        acc += target;

        transform.localPosition = Vector3.Lerp(transform.localPosition, acc, Time.deltaTime * speed);
    }
}
