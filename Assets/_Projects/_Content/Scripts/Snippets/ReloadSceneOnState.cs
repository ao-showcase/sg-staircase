using UnityEngine;
using UnityEngine.SceneManagement;

namespace sg.staircase
{
    public class ReloadSceneOnState : StateMachineBehaviour
    {
        [SerializeField] private string stateName;
    
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (stateInfo.IsName(stateName))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}