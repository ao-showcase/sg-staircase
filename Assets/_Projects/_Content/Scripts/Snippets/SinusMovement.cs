﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinusMovement : MonoBehaviour
{
    [Range(-1, 1)]
    public float xFreq = 1f;
    [Range(-1, 1)]
    public float xFreqFreq = 1f;
    [Range(-1, 1)]
    public float yFreq = 1f;
    [Range(-1, 1)]
    public float yFreqFreq = 1f;
    [Range(0, 10)]
    public float radius = 1f;

    void Update()
    {
        xFreq = Mathf.Sin(Time.time * xFreqFreq);
        yFreq = Mathf.Cos(Time.time * yFreqFreq);

        Vector3 position = new Vector3(Mathf.Sin(Time.time * xFreq), Mathf.Cos(Time.time * yFreq), 0) * radius;
        transform.localPosition = position;
    }
}
