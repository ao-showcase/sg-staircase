using UnityEngine;

namespace sg.staircase
{
    public class FixedScale : MonoBehaviour 
    {
        [SerializeField] private float fixedScale =1 ;
        [SerializeField] private Transform parent;

        private void Update ()
        {
            var parentLocalScale = parent.localScale;
            transform.localScale = new Vector3 (fixedScale / parent.transform.localScale.x,fixedScale / parentLocalScale.y,fixedScale / parentLocalScale.z);
        }
    }
}