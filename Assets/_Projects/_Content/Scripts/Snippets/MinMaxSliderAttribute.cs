using System;
using UnityEngine;

namespace sg
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class MinMaxSliderAttribute : PropertyAttribute
    {
        public readonly float min;
        public readonly float max;

        public MinMaxSliderAttribute() : this(0, 1) {}

        public MinMaxSliderAttribute(float min, float max)
        {
            this.min = min;
            this.max = max;
        }
    }
    
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class MinMaxIntSliderAttribute : PropertyAttribute
    {
        public readonly int min;
        public readonly int max;

        public MinMaxIntSliderAttribute() : this(0, 1) {}

        public MinMaxIntSliderAttribute(int min, int max)
        {
            this.min = min;
            this.max = max;
        }
    }
}