using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UnityStandardAssets.Utility
{
    [RequireComponent(typeof (Text))]
    public class FPSCounter : MonoBehaviour
    {
        const float fpsMeasurePeriod = 0.5f;
        private int m_FpsAccumulator = 0;
        private float m_FpsNextPeriod = 0;
        private int m_CurrentFps;
        const string display = "{0}";
        private Text m_Text;

        private int historyMax = 6;
        private int historyCounter = 0;
        private float fpsSum;

        private float averageFPS = 0;

        private void Start()
        {
            m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
            m_Text = GetComponent<Text>();
        }


        private void Update()
        {
            // measure average frames per second
            m_FpsAccumulator++;
            if (Time.realtimeSinceStartup > m_FpsNextPeriod)
            {
                m_CurrentFps = (int) (m_FpsAccumulator/fpsMeasurePeriod);
                m_FpsAccumulator = 0;
                m_FpsNextPeriod += fpsMeasurePeriod;
                m_Text.text = string.Format(display, m_CurrentFps);

                historyCounter++;
                fpsSum += m_CurrentFps;

                if (historyCounter >= historyMax)
                {
                    historyCounter = 0;
                    averageFPS = fpsSum / historyMax;
                    fpsSum = 0;
                    
                    //Debug.Log(averageFPS);
                }
            }
        }
        
        public float GetFPS()
        {
            return averageFPS;
        }
    }
}
