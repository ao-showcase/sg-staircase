using UnityEngine;

namespace sg.staircase
{
    public class DestroyOnState : StateMachineBehaviour
    {
        [SerializeField] private string stateName;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (stateInfo.IsName(stateName))
            {
                Destroy(animator.gameObject);
            }
        }
    }
}